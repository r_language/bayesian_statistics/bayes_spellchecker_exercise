# Bayes_Spellchecker_Exercise

In this rmd file you can have a look at an exercise where we used Bayes method. The exercise is the following : 

We write the word "radom", how should it be read ? 

Let's imagine only 3 possibilites of correction "random", "radon" or "radom".
We write $\theta$ the words that the user wanted to type, we have 3 possibilities for $\theta$, $\theta \in \{\theta_1, \theta_2, \theta_3\}$, where $\theta_1$, $\theta_2$ and $\theta_3$ correspond to the 3 possible words.

We would like to calculate $\forall j \in \{1,2,3\}$, $p(\theta_j | "radom")$. That's why we can use Bayes formula because we have to cmbine a priori information (frequency of appearance of the different words) and the likelihood of the typed word.

We have the following tab with the frequencies of those 3 words in english language :


|$\theta$   | $p(\theta)$   |
|-----------|---------------|
|random     | $7,60.10^{-5}$|
|radon      | $6,05.10^{-6}$|
|radom      | $3,12.10^{-7}$|


Google estimates the following being the probabilities to make a typing error :

p('radom'|\random) = 0,00193

p('radom'|\radon) = 0,000143

p('radom'|\radom) = 0,975


*Question 1* : What is the a posteriori probability for each word "random", "radon" and "radom" ?

*Question 2* : What would be the word given by the spellchecker ?

*Question 3* : Another possibility rather than Bayes could be conceivable ?

Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2022





